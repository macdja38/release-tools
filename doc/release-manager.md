# Release Manager

This documentation has been moved to <https://gitlab.com/gitlab-org/release/docs/blob/master/quickstart/release-manager.md>.

---

[Return to Guides](../README.md#guides)
