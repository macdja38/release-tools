# Pushing to multiple remotes

This documentation has been moved to <https://gitlab.com/gitlab-org/release/docs/blob/master/general/push-to-multiple-remotes.md>.

---

[Return to Guides](../README.md#guides)
