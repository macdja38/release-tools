## First steps

Stable branch should be created after the 7th. The 7th is the last date to reliably get things in.

- [ ] Create the <%= PickIntoLabel.for(version) %> group label if it doesn't exist: https://gitlab.com/groups/gitlab-org/labels/new
  - Title: `Pick into <%= version.to_minor %>`
  - Description: ``Merge requests to cherry-pick into the `<%= version.stable_branch %>` branch.``
  - Color: `#00c8ca`
- [ ] Make sure the latest [CE to EE] on `master` branches is merged (it will reduce conflicts in the future). Ask in `#ce-to-ee` when in doubt.
- [ ] Create branch `<%= version.stable_branch %>` from CE `master` manually
- [ ] Create branch `<%= version.stable_branch(ee: true) %>` from EE `master` manually
- [ ] In Omnibus create both `<%= version.stable_branch %>` and `<%= version.stable_branch(ee: true) %>` from `master` manually
- [ ] Merge GitLab CE into EE on stable branches following the [Merging a CE stable branch into its EE counterpart] guide
- [ ] Sync stable branches: CE, EE, and Omnibus to `dev`, CE and Omnibus to `github`
- [ ] Sync master branches to `dev` and `github`, as the CHANGELOG will be automatically updated on master during tagging
- [ ] If needed, sync tags for dependencies (`gitlab-shell`, `gitlab-workhorse`, `gitlab-pages`, `gitaly`) to `dev` and `github` (when applicable. Builds should fail if this is needed.)

[CE to EE]: https://gitlab.com/gitlab-org/gitlab-ee/merge_requests?label_name[]=CE+upstream&scope=all&state=opened
[Merging a CE stable branch into its EE counterpart]: https://gitlab.com/gitlab-org/release/docs/blob/master/general/merge-ce-into-ee.md#merging-a-ce-stable-branch-into-its-ee-counterpart

## RC1

- Follow the [Creating RC1] guide:
  - [ ] Create an MR on **CE** master updating the "Installation from Source" guide, creating the "Update" guides
  - [ ] Create an MR on **EE** master creating the "CE to EE" guides
  - [ ] Create an MR on **CE** master updating the `.gitignore`, `.gitlab-ci.yml`, and `Dockerfile` templates
  - [ ] Create an MR on **CE** master updating the dependencies license list
  - [ ] Ensure the above MRs are merged and marked <%= PickIntoLabel.for(version) %>
  - [ ] Create a task list for RC1:

      ```sh
      # In the release-tools project:
      bundle exec rake "patch_issue[<%= version.to_rc(1) %>]"
      ```
  - [ ] Link the RC1 issue as a related issue in this issue

[Creating RC1]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc/release-candidates.md#creating-rc1

## Subsequent RCs

- [ ] Update the blog post `upgrade barometer` section with the migration types for this release, including the time they took to complete.
- Create additional release candidates as needed:

    ```sh
    # In the release-tools project (update RC number):
    bundle exec rake "patch_issue[<%= version.to_rc(2) %>]"
    ```
- [ ] Link any subsequent RCs issues as a related issue in this issue

Keep in mind that:

1. After the feature freeze only regression and security fixes should be
   cherry-picked into stable branches.
2. The final RC should point to the same commit as the final release.

## On the 7th
- [ ] In `#development`:

    ```
    @channel

    `<%= version.stable_branch %>` branch has been created. Everything merged into `master`
    after this point will go into next month's release. Only regression and security fixes
    will be cherry-picked into `<%= version.stable_branch %>`.

    Please ensure that merge requests have the correct milestone (`<%= version.to_minor %>` for this release)
    and the `Pick into <%= version.to_minor %>` label.

    From now on, please follow the "After the 7th" process:
    https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#after-the-7th
    ```

## Final RC

The final RC should be created on the 21st of the month.

### Before 13:00 UTC

Including changes at this stage requires signoff from the VP of Engineering.

- [ ] Create a task list for the final RC:

    ```sh
    # In the release-tools project (update RC number):
    bundle exec rake "patch_issue[<%= version.to_rc(22) %>]"
    ```

### At 15:00 UTC

If the final RC isn't tagged and deployed by this time, notify the
[Distribution Lead][getting help].

### At 20:00 UTC

If the final RC still isn't tagged and deployed by this time, notify the
[VP of Engineering][getting help].

## 22nd: release day

No new code can added to the release that was not included in the final RC.

- At 08:00 UTC, final release is ready for tagging (Including changes at this stage requires signoff from the [VP of Engineering][getting help]):
  - [ ] Ensure tests are green on [CE stable branch]
  - [ ] Ensure tests are green on [EE stable branch]
  - [ ] Ensure tests are green on [Omnibus CE stable branch]
  - [ ] Ensure tests are green on [Omnibus EE stable branch]
  - [ ] Sync stable branches: CE, EE, and Omnibus to `dev`, CE and Omnibus to `github`
  - [ ] Sync master branches to `dev` and `github`, as the CHANGELOG will be automatically updated on master during tagging

- Before 10:00 UTC:
  - [ ] Tag the `<%= version.to_patch %>` version using the [`release` task]:

        ```sh
        # In the release-tools project:
        bundle exec rake "release[<%= version.to_patch %>]"
        ```
  - [ ] Check progress of [EE packages build](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: true) %>) and [CE packages build](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: false) %>)
  - [ ] Warm up the packages on takeoff by running:
        ```sh
        # In the takeoff project:
        bin/takeoff-deploy -v <%= version.to_patch %>-ee.0 -w
        ```
- Before 12:00 UTC:
  - [ ] Notify #production channel about staging deploy. No need to require confirmation.
  - [ ] On video call, [deploy] `<%= version.to_patch %>` to [staging.gitlab.com]

        ```sh
        # In the takeoff project:
        bin/takeoff-deploy -e staging -v <%= version.to_patch %>-ee.0
        ```
  - [ ] Notify #production channel about canary deploy. No need to require confirmation.
  - [ ] On video call, [deploy] `<%= version.to_patch %>` to [canary.gitlab.com]

        ```sh
        # In the takeoff project:
        bin/takeoff-deploy -e canary -v <%= version.to_patch %>-ee.0
        ```
  - [ ] Get confirmation from a production team member to deploy production. Use `!oncall prod` if needed to find who's on call. If someone besides the oncall confirms, `@mention` the oncall so they are aware.
  - [ ] On video call, [deploy] `<%= version.to_patch %>` to GitLab.com

        ```sh
        # In the takeoff project:
        bin/takeoff-deploy -e production -v <%= version.to_patch %>-ee.0
        ```
- At 14:30 UTC:
  - [ ] From the [build pipeline], [manually publish public packages]
  - Make sure that neither packages nor the blog post get published early without approval by the marketing team
- At 15:00 UTC:
  - [ ] Verify that packages appear on `packages.gitlab.com`: [EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=<%= version.to_patch %>) / [CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=<%= version.to_patch %>)
  - [ ] Verify that Docker images appear on `hub.docker.com`: [EE](https://hub.docker.com/r/gitlab/gitlab-ee/tags) / [CE](https://hub.docker.com/r/gitlab/gitlab-ce/tags)
  - [ ] Create the `<%= version.to_patch %>` version on [version.gitlab.com](https://version.gitlab.com/versions/new?version=<%= version.to_patch %>)
  - [ ] Ensure someone tweets about the `<%= version.to_patch %>` release in the `#releases` channel

[CE stable branch]: https://gitlab.com/gitlab-org/gitlab-ce/commits/<%= version.stable_branch %>
[EE stable branch]: https://gitlab.com/gitlab-org/gitlab-ee/commits/<%= version.stable_branch(ee: true) %>
[Omnibus CE stable branch]: https://gitlab.com/gitlab-org/omnibus-gitlab/commits/<%= version.stable_branch %>
[Omnibus EE stable branch]: https://gitlab.com/gitlab-org/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>
[announce the deployment]: https://gitlab.com/gitlab-org/takeoff/blob/master/doc/announce-a-deployment.md
[`release` task]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc%2Frake-tasks.md#releaseversion
[deploy]: https://gitlab.com/gitlab-org/takeoff#deploying-gitlab
[staging.gitlab.com]: https://staging.gitlab.com/
[canary.gitlab.com]: https://canary.gitlab.com/
[manually publish public packages]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc/publishing-packages.md
[build pipeline]: https://dev.gitlab.org/gitlab/omnibus-gitlab/pipelines?scope=tags
[getting help]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc/monthly.md#getting-help

/milestone %"<%= version.to_minor %>"
