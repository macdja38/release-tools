# Security Releases

This documentation has been moved to <https://gitlab.com/gitlab-org/release/docs/blob/master/general/security.md>.

---

[Return to Guides](../README.md#guides)
