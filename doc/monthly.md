# Monthly Release

This documentation has been moved to <https://gitlab.com/gitlab-org/release/docs/blob/master/general/monthly.md>.

---

[Return to Guides](../README.md#guides)
