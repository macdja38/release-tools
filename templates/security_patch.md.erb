**General guidelines**

- Be sure to follow the [Security Releases guide](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security.md).
- Always work on [https://dev.gitlab.org/](https://dev.gitlab.org/). Do not push anything to [https://gitlab.com](https://gitlab.com)
- Deployment tasks (staging, canary, and production) should be done only if the patch is for the latest version. Feel free to delete those sections if they don't apply.

## Preparation

- Picked into respective `stable` branches from the `dev/security` branch. Use the <SECURITY_RELEASE_ISSUE> as a guideline of what MR's should be picked.
- [ ] **Push `ce/<%= version.stable_branch %>` to `dev` only: `git push dev <%= version.stable_branch %>`**
- [ ] **Push `ee/<%= version.stable_branch(ee: true) %>` to `dev` only: `git push dev <%= version.stable_branch(ee: true) %>`**
- [ ] Merge `ce/<%= version.stable_branch %>` into `ee/<%= version.stable_branch(ee: true) %>` 

## Packaging

- [ ] Make sure [`omnibus-gitlab/<%= version.stable_branch %>` CHANGELOG.md][omnibus-stable-changelog] has an entry for each introduced change on [Omnibus CE stable branch]
- [ ] Make sure [`omnibus-gitlab/<%= version.stable_branch(ee: true) %>` CHANGELOG.md][omnibus-stable-ee-changelog] has an entry for each introduced change on [Omnibus EE stable branch]
- [ ] **Push `omnibus-gitlab/<%= version.stable_branch %>` to `dev` only: `git push dev <%= version.stable_branch %>`**
- [ ] **Push `omnibus-gitlab/<%= version.stable_branch(ee: true) %>` to `dev` only: `git push dev <%= version.stable_branch(ee: true) %>`**
- [ ] Ping the Security Engineers so they can get started with the blog post. The blog post should also be done on https://dev.gitlab.org/ in a **private snippet**: BLOG_POST_SNIPPET

- [ ] Ensure [tests are green on CE]
- [ ] Ensure [tests are green on EE]

- [ ] Check for any problematic migrations in EE (EE migrations include CE ones), and paste the diff in a snippet: `git diff -M <%= version.previous_tag(ee: true) %>..<%= version.stable_branch(ee: true) %> -- db/migrate db/post_migrate ee/db/migrate ee/db/post_migrate` =>
- [ ] Tag the `<%= version.to_patch %>` version using the [`release` task]:

      ```sh
      SECURITY=true bundle exec rake "release[<%= version.to_patch %>]"
      ```
- [ ] Check that [EE packages are built] and [CE packages are built]

## Deploy

- [ ] Warm up the packages on takeoff by running:

      ```sh
      # In the takeoff project:
      bin/takeoff-deploy -v <%= version.to_patch %>-ee.0 -w
      ```

### staging.gitlab.com

- [ ] Notify #production channel about staging deploy.
- [ ] On video call, deploy `<%= version %>` to [staging.gitlab.com]

      ```sh
      # In the takeoff project:
      bin/takeoff-deploy -e staging -v <%= version.to_patch %>-ee.0
      ```

- [ ] Although normally there aren't any schema changes on a security release, make sure that the database migrations complete in a timely manner. If you see a migration taking too long in staging, stop right there and ping `@db-team` in Slack. What you're witnessing has the potential to take down production for a long time. The statement "Don't worry: it'll be faster in production" is a false myth that has been disproven every single time that this has happened in the past.

#### QA 

- [ ] Create a "QA Task" issue in the [gitlab-org/release/tasks](https://gitlab.com/gitlab-org/release/tasks) repo, using the `QA-task` template. 
- [ ] Notify #development and #releases about the QA issue with the following message: `<%= version.to_patch %> has been deployed to staging, you can do QA and report any problems to the QA issue <link to the QA issue>`
- [ ] Wait for the QA Task deadline to pass.

### canary.gitlab.com

- [ ] Notify #production channel about canary deploy.
- [ ] On video call, deploy `<%= version %>` to [canary.gitlab.com]

      ```sh
      # In the takeoff project:
      bin/takeoff-deploy -e canary -v <%= version.to_patch %>-ee.0
      ```

### gitlab.com (production)

- [ ] Get confirmation from a production team member to deploy production. Use `!oncall prod` if needed to find who's on call. If someone besides the oncall confirms, `@mention` the oncall so they are aware.
- [ ] On video call, deploy `<%= version %>` to [GitLab.com]

      ```sh
      # In the takeoff project:
      bin/takeoff-deploy -e production -v <%= version.to_patch %>-ee.0
      ```

### Release 

- [ ] This section should be done in coordination with the Security team, so **make sure to confirm with them before proceeding**
- [ ] From the [build pipeline], [manually publish public packages]
- [ ] Create the `<%= version %>` version on [version.gitlab.com](https://version.gitlab.com/versions/new?version=<%= version %>). **Be sure to mark it as a security release.**
- [ ] Push `ce/<%= version.stable_branch %>` to all remotes
- [ ] Push `ee/<%= version.stable_branch(ee: true) %>` to all remotes
- [ ] Push `omnibus/<%= version.stable_branch %>` and `omnibus/<%= version.stable_branch(ee: true) %>` to all remotes
- [ ] Push CE, EE and omnibus tags to all remotes
- [ ] Tweet (prepare the Tweet text below or paste the tweet URL instead) in the `#releases` channel:

      ```
      !tweet "GitLab <%= version %> is released! BLOG_POST_URL DESCRIPTION OF THE CHANGES"
      ```


- [ ] Merge the MR's targeting `master` and push to all remotes
- [ ] Add [`omnibus-gitlab/<%= omnibus_version.tag %>` CHANGELOG.md][omnibus-tag-changelog] items to [`omnibus-gitlab/master` CHANGELOG.md][omnibus-master-changelog]

---

For references:
- https://dev.gitlab.org/gitlab/gitlab-ee/commits/<%= version.stable_branch(ee: true) %>
- https://dev.gitlab.org/gitlab/gitlabhq/commits/<%= version.stable_branch %>
- https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>
- https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>

[build pipeline]: https://dev.gitlab.org/gitlab/omnibus-gitlab/pipelines?scope=tags
[manually publish public packages]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc/publishing-packages.md
[omnibus-stable-changelog]: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/<%= version.stable_branch %>/CHANGELOG.md
[omnibus-stable-ee-changelog]: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/<%= version.stable_branch(ee: true) %>/CHANGELOG.md

[tests are green on CE]: https://dev.gitlab.org/gitlab/gitlabhq/commits/<%= version.stable_branch %>
[tests are green on EE]: https://dev.gitlab.org/gitlab/gitlab-ee/commits/<%= version.stable_branch(ee: true) %>
[EE packages are built]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/8-10-stable-ee
[CE packages are built]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/8-10-stable

[omnibus-tag-changelog]: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/<%= omnibus_version.tag %>/CHANGELOG.md
[omnibus-master-changelog]: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/CHANGELOG.md

[Omnibus CE stable branch]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>
[Omnibus EE stable branch]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>

[EE packages are built]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>
[CE packages are built]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>

[`gitlab/gitlab-ee`]: https://packages.gitlab.com/gitlab/gitlab-ee
[`gitlab/gitlab-ce`]: https://packages.gitlab.com/gitlab/gitlab-ce

[`release` task]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc%2Frake-tasks.md#releaseversion
[`patch_issue` task]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc%2Frake-tasks.md#patch_issueversion

[staging.gitlab.com]: https://gitlab.com/gitlab-org/takeoff#deploying-gitlab
[canary.gitlab.com]: https://canary.gitlab.com/
[GitLab.com]: https://gitlab.com/gitlab-org/takeoff#deploying-gitlab

[publicly acknowledged]: https://about.gitlab.com/vulnerability-acknowledgements/

[publish the packages]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc/publishing-packages.md
